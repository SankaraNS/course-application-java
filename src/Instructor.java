/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;

/**
 **Represents an Instructor who teaches students
 * particular courses and is employed by an Institution
 * @author Sanka
 */
public class Instructor 
{
    private String firstName, lastName, streetAddress ,city, postalCode;
    private int employeeNum;
    private LocalDate birthday, hireDate;
    private ArrayList<String> courseCode = new ArrayList() ;

    /**
     *Constructor of Instructor class which accepts the below parameters
     * @param firstName
     * @param lastName
     * @param streetAddress
     * @param city
     * @param postalCode
     * @param hireDate
     * @param birthday
     * @param employeeNum
     */
    public Instructor(String firstName, String lastName, String streetAddress, String city, String postalCode, LocalDate hireDate, LocalDate birthday, int employeeNum)
    {
        
        setFirstName(firstName);
        setLastName(lastName);
        setStreetAddress(streetAddress);
        setCity(city);
        setPostalCode(postalCode);
        setEmployeeNum(employeeNum);
        setBirthday(birthday);
        setHireDate(hireDate);
    }

    /**
     *toString() function that returns string representation of the object of the Instructor class
     * @return Instructor
     */
    @Override
    public String toString() 
    {
        return firstName+" "+lastName;
    }

    /**
     *function to return First name of Instructor
     * @return firstName
     */
    public String getFirstName() 
    {
        return firstName;
    }

    /**
     *method that initializes the first name of the instructor
     * @param firstName
     */
    public void setFirstName(String firstName) 
    {
        if(firstName!=null && !firstName.isEmpty())
        {
        this.firstName = firstName;
        }
        else
        {
            throw new IllegalArgumentException("first name cannot be empty");
        }
    }

    /**
     *function to return the last name of the instructor
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * method to initialize the last name of the Instructor
     * @param lastName
     */
    public void setLastName(String lastName) 
    {
        if(!lastName.isEmpty() && lastName != null)
        {
           this.lastName = lastName; 
        }
        else
        {
           throw new IllegalArgumentException("Last name cannot be empty");
        }
        
    }

    /**
     *get function to return the street address of the instructor
     * @return streetAddress
     */
    public String getStreetAddress() 
    {
        return streetAddress;
    }

    /**
     *set method to initialize street address of Instructor and checks if the string is empty
     * @param streetAddress
     */
    public void setStreetAddress(String streetAddress) 
    {
        if(!streetAddress.isEmpty() && streetAddress != null)
        {
        this.streetAddress = streetAddress;
        }
         else
        {
            throw new IllegalArgumentException("Street address cannot be empty");
        }
    }

    /**
     *get function to return city of instructor
     * @return city
     */
    public String getCity() 
    {
        return city;
    }

    /**
     *set method to initialize city and checks if city is empty
     * @param city
     */
    public void setCity(String city) 
    {
        if(!city.isEmpty() && city != null)
        {
           this.city = city; 
        }
         else
        {
            throw new IllegalArgumentException("City cannot be empty");
        }
        
    }

    /**
     *get function to return postal code of instructor
     * @return  postalCode
     */
    public String getPostalCode() 
    {
        return postalCode;
    }

    /**
     *set method to initialize postal code of instructor and checks if input string is empty
     * @param postalCode
     */
    public void setPostalCode(String postalCode) 
    {
        if(!postalCode.isEmpty() && postalCode != null)
        {
        this.postalCode = postalCode;
        }
        else
        {
            throw new IllegalArgumentException("Postal Code cannot be empty");
        }
    }

    /**
     *get function to return the employee number
     * @return employeeNum
     */
    public int getEmployeeNum() {
        return employeeNum;
    }

    /**
     *set method to initialize employee number and checks if its greater than 0
     * @param employeeNum
     */
    public void setEmployeeNum(int employeeNum) 
    {
        if((employeeNum>0))
        {
            this.employeeNum = employeeNum;
        }
        else
        {
            throw new IllegalArgumentException("Employee number must be above 0");
        }
    }

    /**
     *get function to return birthday of Instructor
     * @return birthday
     */
    public LocalDate getBirthday() 
    {
        return birthday;
    }

    /**
     *set method to initialize birth date and checks if its in the past and 
     * that the date is not more than 90 years before the current date
     * @param birthday
     */
    public void setBirthday(LocalDate birthday) 
    {
       
       LocalDate now;
       now= LocalDate.now();
        if (Period.between(birthday, now).getYears()<=90 )//&& birthday.isBefore(now))
        {
             this.birthday = birthday;
        }
        else
        {
            throw new IllegalArgumentException("age is above 90");
        }
        
    }

    /**
     *get function to return the hire date of the instructor
     * @return hireDate
     */
    public LocalDate getHireDate() 
    {
        
        return hireDate;
    }

    /**
     *set function to initialize the hire date of instructor and checks if it's not more
     * than one month in the future and it's not more than 90 years in the past.
     * @param hireDate
     */
    public void setHireDate(LocalDate hireDate) 
    {
       LocalDate now;
       now= LocalDate.now();
       if((Period.between(now, hireDate).getMonths()<=1)&& (Period.between(now, hireDate).getYears()<=90))
       {
            this.hireDate = hireDate;
       }
       else
       {
            throw new IllegalArgumentException("hire date cannot be more than 1 month in the future");
       }
        
    }
    
    /**
     *get function to return the birth year of the instructor
     * @return birthday
     */
    public int getYearBorn() 
    {
        return birthday.getYear();
    }
    
    /**
     *get function to return the age of the instructor in years.
     * @return age
     */
    public int getAgeInYears()
    {
        LocalDate now;
        now= LocalDate.now();
        int age;
        age=Period.between (now,birthday).getYears();
        age= Math.abs(age);
        return age;
        
    }
    
    /**
     *get function to return the number of years the instructor has
     * been employed at the college
     * @return numberOfYears
     */
    public int getYearsAtCollege()
    {
        LocalDate now;
        now= LocalDate.now();
        int numberOfYears;
        numberOfYears=Period.between(hireDate, now).getYears();
        numberOfYears=Math.abs(numberOfYears);
        return numberOfYears;
    }
    
    /**
     *method to add a course that the Instructor can teach in upper case
     * @param codeOfCourse
     */
    public void addCourseToAbilities(String codeOfCourse) 
    {
       if(!codeOfCourse.isEmpty() && codeOfCourse!=null)
       {
           String codeOfCourseUC = codeOfCourse.toUpperCase();
           courseCode.add(codeOfCourseUC);
           
       }
       else
       {
          throw new IllegalArgumentException("Course Code cannot be empty"); 
       } 
    }
    
    /**
     *function that checks if a Instructor can teach a particular course
     * @param codeOfCourse
     * @return
     */
    public boolean canTeach(String codeOfCourse)
    {
        if(!codeOfCourse.isEmpty() && codeOfCourse != null)
       {
           codeOfCourse = codeOfCourse.toUpperCase();
           
           if(courseCode.contains(codeOfCourse))
           {
               return true;
           }
           
           else
           {
               return false;
           }
       }
        else
        {
            throw new IllegalArgumentException("Course Code cannot be empty");
        }
    }
    
    /**
     *a function that shows the list of subjects that an Instructor can teach
     * @return
     */
    public String listOfSubjectsCertifiedToTeach()
    {
        String courseList = "";
        if(courseCode!=null)
        {
            for(int i=0; i<courseCode.size(); i++)
            {
                if(i == courseCode.size()-1)
                {
                        courseList += courseCode.get(i);
                }
                else
                {
                    courseList += courseCode.get(i) + ", ";
                }
            }
        }
            
        else
        {
        throw new IllegalArgumentException("Course Code cannot be empty");
        }
        
        return courseList;
    }
}
            
        
    
    
    
    
    

