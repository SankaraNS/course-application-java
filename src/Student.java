import java.time.LocalDate;
import java.time.Period;

/**
 **Represents a Student who can be enrolled in multiple courses
 * and can be taught by Instructors
 * @author Sanka
 */

public class Student 
{
    
    private String firstName, lastName, streetAddress ,city, postalCode;
    private int studentId;
    private LocalDate dateOfBirth, dateEnrolled;
    private Boolean goodStanding = true;

 
 /**
     * constructor with all parameters and checks data when entering
     * @param firstName
     * @param lastName
     * @param streetAddress
     * @param city
     * @param postalCode
     * @param dateOfBirth
     * @param DateEnrolled
     * @param studentId
     */
    public Student(String firstName, String lastName, String streetAddress, String city, String postalCode, LocalDate dateOfBirth, LocalDate DateEnrolled, int studentId )
 {
     setFirstName(firstName);
     
     setLastName(lastName);
     
     setStreetAddress(streetAddress);
     
     setCity(city);
     
     setPostalCode(postalCode);
     
     setDateOfBirth(dateOfBirth);
    
    if(studentId > 0)
    {this.studentId= studentId;}
    else
    {throw new IllegalArgumentException("student id should be above 0");}   
    
    setDateEnrolled(DateEnrolled);
 } 
    
    /**
     *returns the year a Student was born
     * @return date of birth
     */
    public int getYearBorn()
   {
      return dateOfBirth.getYear();
   }
   
    /**
     *returns the current age of the student
     * @return  current age
     */
    public int getAge()
   {
       
    return Period.between(dateOfBirth,LocalDate.now()).getYears();
       
   }
   
    /**
     *validates whether the person is less than 100 years of age.
     * throws IllegalArgumentException if birthday doesn't appear to be valid
     * @param dateOfBirth
     */
    public void setDateOfBirth(LocalDate dateOfBirth) 
    {
        int age = Period.between(dateOfBirth, LocalDate.now()).getYears();
      if (age > 100)
          throw new IllegalArgumentException("Age of student should be less than 100");
      else
        this.dateOfBirth = dateOfBirth;
    }
    
    /**
     *validates student is not enrolled in future
     * @param enrollDate
     */
    public void setDateEnrolled(LocalDate enrollDate)    
   {
       LocalDate now;
       now= LocalDate.now();
       
       if(enrollDate.isBefore(LocalDate.now()))  
       {
          this.dateEnrolled = enrollDate;
           
       }
      else
      {
          throw new IllegalArgumentException("Student cannot be enrolled in future");  
      }
   }

    /**
     *returns year of enrollment
     * @return enrollment year
     */
    public int getYearEnrolled() 
    {
        return dateEnrolled.getYear();
    }
   
    /**
     *checks f student is in good standing
     * @return true or false
     */
    public boolean isInGoodStanding()
   {
       if (goodStanding == true)
       {
           return true;
       }
       else
       {
           return false;
       }
           
                   
   }
   
    /**
     *sets goodStanding to false
     */
    public void suspendStudent() 
   {
       goodStanding=false;
   }
   
    /**
     *Sets goodStanding from false to true.
     */
    public void reinstateStudent()
   {
       goodStanding=true;
   }
   
    /**
     * toString function that returns first name, lastname and student number of student
     * @return firstName, lastName and studentId
     */
    @Override
   public String toString()
   {
       return firstName+" "+lastName+", student number: "+studentId;
   }

    /**
     *returns firstName
     * @return firstName
     */
    public String getFirstName() 
    {
        return firstName;
    }

    /**
     * sets first name
     * @param firstName
     */
    
    public void setFirstName(String firstName)
    {
        if(firstName != null && !firstName.isEmpty())
        {
            this.firstName = firstName;
        }
        else
        {
            throw new IllegalArgumentException("first name cannot be empty");
        }
    }

    /**
     *returns lastName
     * @return lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     *sets last name
     * @param lastName
     */
    public void setLastName(String lastName) 
    {
        if(lastName != null && !lastName.isEmpty())
        {
            this.lastName = lastName;
        }
        else
        {
            throw new IllegalArgumentException("Last name cannot be empty");
        }
    }
    

    /**
     *gets street address
     * @return streetAddress
     */
    public String getStreetAddress() 
    {
        return streetAddress;
    }

    /**
     * sets street address
     *
     * @param streetAddress
     */
    public void setStreetAddress(String streetAddress) 
    {
        if(!streetAddress.isEmpty())
        {
            this.streetAddress = streetAddress;
        }
        else
        {
            throw new IllegalArgumentException("street address cannot be empty");
        }
    }

    /**
     *gets city name
     * @return city
     */
    public String getCity() 
    {
        return city;
    }

    /**
     *set's city name
     * @param city
     */
    public void setCity(String city) 
    {
        if(!city.isEmpty())
        {
           this.city = city; 
        }
        else
        {
            throw new IllegalArgumentException("city cannot be empty");
        }
        
    }

    /**
     *gets postal code
     * @return postCode
     */
    public String getPostalCode() 
    {
        return postalCode;
    }

    /**
     *sets postal code
     * @param postalCode
     */
    public void setPostalCode(String postalCode) 
    {
        if(postalCode != null && !postalCode.isEmpty())
        {
            this.postalCode = postalCode;
        }
        else
        {
            throw new IllegalArgumentException("postal code cannot be empty");
        }
    }

    /**
     *gets date of birth
     * @return dateOfBirth
     */
    public LocalDate getDateOfBirth() 
    {
        return dateOfBirth;
    }
          
    /**
     *returns student ID
     * @return studentId
     */
    public int getStudentNum()
    {
        return studentId;
    }
    
    /**
     * returns date enrolled
     *
     * @return DateEnrolled
     */
    public LocalDate getDateEnrolled()
    {
       return dateEnrolled;
    }
 
 
 
 
 
 
}
