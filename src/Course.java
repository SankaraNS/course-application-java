/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;


/**
 *Represents a Course in which a student can be enrolled
 * and an Instructor can teach
 * @author Sanka
 */
public class Course 
{ 
    private Instructor instructor;
    private String courseCode;
    private String courseName;
    private String room;
    private ArrayList<Student> defaultStudent = new ArrayList<>();
    private int maxNumberOfStudents;

    /**
     *Instructor for Class Course which accepts the below as parameters
     * @param defaultInstructor
     * @param courseCode
     * @param courseName
     * @param roomNumber
     * @param maxStudents
     */
    public Course(Instructor defaultInstructor, String courseCode, String courseName, String roomNumber, int maxStudents) 
    
    {
        
        setMaxStudents(maxStudents);
        setCourseCode(courseCode);
        setInstructor(defaultInstructor);
        setCourseName(courseName);
        setRoom(roomNumber);
        
       
    }

    /**
     *a get function to return the ArrayList of students
     * @return defaultStudent
     */
    public ArrayList<Student> getStudents() 
    {
        return defaultStudent;
    }

    /**
     *a get function to return an object of Instructor
     * @return instructor
     */
    public Instructor getInstructor() 
    {
        return instructor;
    }

    /**
     *a set method to initialize Instructor and checks whether the instructor
     * can teach the course before adding
     * @param instructor
     */
    public void setInstructor(Instructor instructor) 
    {
        
        if(instructor.canTeach(courseCode))
        {
            this.instructor = instructor;   
        }
        else
        {
            throw new IllegalArgumentException("The Instructor does not teach this course");
        }
        
    }

    /**
     *a get function to return the course code of the course
     * @return courseCode
     */
    public String getCourseCode() 
    {
        return courseCode;
    }

    /**
     *set method to initialize the course code and checks if its empty before initializing
     * @param courseCode
     */
    public void setCourseCode(String courseCode) 
    {
        if(!courseCode.isEmpty() && courseCode != null)
        
        {
            this.courseCode = courseCode;
        }
        
        else
            
        {
            throw new IllegalArgumentException("The Instructor does not teach this course");
        }
        
    }

    /**
     *get function to return course name
     * @return courseName
     */
    public String getCourseName() 
    {
        return courseName;
    }

    /**
     *set method to initialize course name and checks 
     * whether the string is empty for initializing
     * @param courseName
     */
    public void setCourseName(String courseName) 
    {
        if(!courseName.isEmpty() && courseName != null)
        {
            this.courseName = courseName;
        }
        else
        {
            throw new IllegalArgumentException("Course name is empty");
        }
    }

    /**
     *get function that returns the room number assigned to the course
     * @return room
     */
    public String getRoom() 
    {
        return room;
    }

    /**
     *set method to initialize the room and checks if its empty before 
     * initializing
     * @param room
     */
    public void setRoom(String room) 
    {
        if(!room.isEmpty() && room != null)
        {
            this.room = room;
        }
        else
        {
           throw new IllegalArgumentException("Room number cannot be empty");
        }
        
    }
    
    /**
     * method to show the list of students enrolled in this course
     */
    public void showClassList() 
    {
        for(Student student : defaultStudent)
            System.out.println(student);
    }

    /**
     *set method to set the maximum number of students in the course
     * checks if the number given is between 45 and 1 before initializing 
     * @param maxStudents
     */
    public void setMaxStudents(int maxStudents) 
    {
        if(maxStudents<=45 && maxStudents>0)
        {
            this.maxNumberOfStudents = maxStudents;
        }
        else
        {
            throw new IllegalArgumentException("The number should be between 1 and 45");
        }
    }

    /**
     *get function to return the maximum number of students for the course
     * @return maxNumberOfStudents
     */
    public int getMaxStudents() 
    {
        return maxNumberOfStudents;
    }
    
    /**
     *set method to add a student to the class list
     * checks if the student is in good standing and if the course has
     * reached its maximum number of students limit
     * @param student
     */
    public void addStudent(Student student)
    {
        if(student.isInGoodStanding() && (defaultStudent.size()<getMaxStudents()))
        {
            defaultStudent.add(student);
        }
        else
        {
            throw new IllegalArgumentException("Either Student is not in good standing or Class space is full!");
        }
        
    }

    /**
     *toString() function that returns string representation of the object of the class Course
     * @return Course
     
     */
    @Override
    public String toString() {
        return  courseName+" "+courseCode;
    }
    
}
